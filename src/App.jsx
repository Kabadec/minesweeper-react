import {Minesweeper} from "./components/Minesweeper/Minesweeper";
import {Description} from "./components/Description/Description";
import {BodySeparator} from "./components/BodySeparator/BodySeparator";
import {Contacts} from "./components/Contacts/Contacts";
import {VersionHistory} from "./components/VersionHistory/VersionHistory";
import {Main} from "./components/Main/Main";

export const BODY_WIDTH = 1036;

export const App = () => {
    return (
        <Main>
            <div style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                width: "100%"
            }}>
                <Minesweeper/>
            </div>
            <BodySeparator/>
            <Description/>
            <BodySeparator/>
            <Contacts/>
            <BodySeparator/>
            <VersionHistory/>
        </Main>
    )
}