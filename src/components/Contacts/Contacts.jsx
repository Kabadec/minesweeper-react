import TelegramIcon from '@mui/icons-material/Telegram';
import EmailIcon from '@mui/icons-material/Email';
import { ReactComponent as GitLabIcon } from './Images/gitlab_logo.svg';
import { ReactComponent as HeadHunterIcon } from './Images/hh_logo.svg';
import SvgIcon from '@mui/material/SvgIcon';
import {Link, Stack} from "@mui/material";
import Typography from "@mui/material/Typography";

export const Contacts = () => {
    return (
        <div style={{display: "flex", flexDirection: "column", justifyContent: "flex-start", alignItems: "flex-start"}}>
            <Typography variant="h5" gutterBottom sx={{mt: 0, mb: 1}}>
                Контакты
            </Typography>
            <Link
                display="block"
                variant="body1"
                href="https://t.me/Kabadec"
                key="Telegram"
                sx={{mb: 0.5}}
            >
                <Stack direction="row" spacing={1} alignItems="center">
                    <TelegramIcon/>
                    <span>Telegram</span>
                </Stack>
            </Link>
            <Link
                display="block"
                variant="body1"
                href="https://novosibirsk.hh.ru/resume/698edd38ff0bb1653c0039ed1f686c49493144"
                key="HeadHunter"
                sx={{mb: 0.5}}
            >
                <Stack direction="row" spacing={1} alignItems="center">
                    <SvgIcon component={HeadHunterIcon} viewBox="-8 -8 280 280"></SvgIcon>
                    <span>HeadHunter</span>
                </Stack>
            </Link>
            <Link
                display="block"
                variant="body1"
                href="https://gitlab.com/Kabadec"
                key="GitLab"
                sx={{mb: 0.5}}
            >
                <Stack direction="row" spacing={1} alignItems="center">
                    <SvgIcon component={GitLabIcon} viewBox="90 70 200 250"></SvgIcon>
                    <span>GitLab</span>
                </Stack>
            </Link>
            <Link
                display="block"
                variant="body1"
                href="mailto:kochergin38m@gmail.com"
                key="GitLab"
                sx={{mb: 0.5}}
            >
                <Stack direction="row" spacing={1} alignItems="center">
                    <EmailIcon/>
                    <span>kochergin38m@gmail.com</span>
                </Stack>
            </Link>
        </div>
    )
}