import React from "react";
import {Button} from "@mui/material";

const SEPARATOR_HEIGHT = 8;

export const Navigation = () => {

    return <nav style={{
        display: "flex", flexDirection: "column", width: "210px", minWidth: "200px", paddingRight: "25px"
    }}>
        <Button variant="outlined" href="/">Играть</Button>
        <div style={{height: `${SEPARATOR_HEIGHT}px`}}/>
        <Button variant="outlined" href="/" disabled>История матчей TBD</Button>
    </nav>;
};