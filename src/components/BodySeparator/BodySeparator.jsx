import React from "react";

export const BodySeparator = () => {
    return (
        <div style={{
            boxShadow: "0 1px 0 0 #eee inset", height: "5px", width: "100%", margin: "0 0 10px 0"
        }}></div>
    )
}