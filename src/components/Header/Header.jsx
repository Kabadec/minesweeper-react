import React from 'react';
import classnames from "classnames";
import header from "./header.module.css"
import {Box, Button} from "@mui/material";

export const Header = () => {
    return (
        <div style={{
            display: "flex",
            justifyContent: "space-between",
            width: "var(--body-width)",
            maxWidth: "100%",
            minWidth: "460px",
            padding: "10px 0 9px",
            margin: "0 auto"
        }}>
            <div style={{display: "flex"}}>
                <div className={classnames(header.logo)}
                     style={{
                         backgroundSize: "100% 100%",
                         width: "100px",
                         height: "100px",
                         margin: "0px 20px 0px 0px"
                     }}/>
                <div style={{display: "flex", flexDirection: "column", justifyContent: "center"}}>
                    <h1 style={{margin: "0"}}>Minesweeper ASP.NET</h1>
                    <p style={{margin: "0"}}>Пет-проект для портфолио</p>
                </div>

            </div>
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 0.5,
                    alignItems: 'center',
                    justifyContent: 'center',
                    minWidth: "164px"
                }}
            >
                <Box display="flex" flexDirection="row">
                    <Button
                        color="primary"
                        variant="text"
                        size="medium"
                        component="a"
                        href="/"
                        target="_blank"
                        disabled
                    >
                        Sign in
                    </Button>
                    <Button
                        color="primary"
                        variant="contained"
                        size="medium"
                        component="a"
                        href="/"
                        target="_blank"
                        disabled
                    >
                        Sign up
                    </Button>
                </Box>
                <div style={{color: "lightgray"}}>In Progress</div>
            </div>
        </div>
    );
}