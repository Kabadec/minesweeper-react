import React, {useEffect, useState} from 'react'
import versionHistory from './VersionHistory.md'
import ReactMarkdown from "react-markdown";
import Typography from "@mui/material/Typography";

export const VersionHistory = () => {
    const [markdown, setMarkdown] = useState("");

    useEffect(() => {
        fetch(versionHistory)
            .then((res) => res.text())
            .then((text) => setMarkdown(text));
    }, []);
    
    return (
        <div>
            <Typography variant="h5" gutterBottom sx={{mt: 0, mb: 1}}>
                История версий
            </Typography>
            <ReactMarkdown children={markdown}/>
        </div>)
}