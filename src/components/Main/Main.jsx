import {Header} from "../Header/Header";
import {Box, Paper} from "@mui/material";
import {Navigation} from "../Navigation/Navigation";
import {Footer} from "../Footer/Footer";
import React, {useEffect, useState} from "react";
import {BODY_WIDTH} from "../../App";

export const Main = ({children}) => {
    const [availableBodyWidth, setAvailableBodyWidth] = useState(document.documentElement.clientWidth);

    useEffect(() => {
        const handleResize = () => {
            setAvailableBodyWidth(Math.max(BODY_WIDTH, document.documentElement.clientWidth));
        };

        setAvailableBodyWidth(Math.max(BODY_WIDTH, document.documentElement.clientWidth));
        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    return (
        <Box component="main" sx={{
            backgroundColor: (theme) =>
                theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[900],
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            height: "100%",
            width: "100%"
        }}>
            <div style={{width: "100%"}}>
                <div style={{
                    display: "block",
                    width: `${Math.max(BODY_WIDTH, availableBodyWidth)}px`
                }}>
                    <Header/>
                    <Paper sx={{
                        p: 2,
                        display: "flex",
                        width: `${BODY_WIDTH}px`,
                        margin: "0 auto 5px auto",
                        padding: "20px 25px",
                        border: "1px solid #eee"
                    }}>
                        <Navigation/>
                        <div style={{
                            display: "flex",
                            flexGrow: 1,
                            flexDirection: "column",
                            justifyContent: "flex-start",
                            alignItems: "flex-start",
                            paddingLeft: "25px",
                            boxShadow: "1px 0 0 0 #eee inset"
                        }}>
                            {children}
                        </div>
                    </Paper>
                    <Footer/>
                </div>
            </div>
        </Box>
    )
}