import React, {useState} from 'react';
import {useCookies} from 'react-cookie';
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";

export const DIFFICULT_COOKIE_KEY = 'difficult';

export const DifficultMap = {
    BEGINNER: "Beginner",
    INTERMEDIATE: "Intermediate",
    EXPERT: "Expert"
}

export const DifficultSelector = (props) => {
    const onDifficultChange = props.onDifficultChange;
    const [cookies, setCookie] = useCookies([DIFFICULT_COOKIE_KEY]);
    const [difficult, setDifficult] = useState(cookies.difficult || DifficultMap.BEGINNER);

    const handleSelectChange = (e) => {
        const value = e.target.value;
        setDifficult(value);
        setCookie(DIFFICULT_COOKIE_KEY, value, {path: '/'});
        onDifficultChange(value);
    };

    return (
        <FormControl sx={{m: 1, maxWidth: 150}} size="small">
            <InputLabel id="demo-select-small-label">Difficult</InputLabel>
            <Select
                labelId="demo-select-small-label"
                id="demo-select-small"
                value={difficult}
                label="Difficult"
                onChange={handleSelectChange}
            >
                <MenuItem value={DifficultMap.BEGINNER}>{DifficultMap.BEGINNER}</MenuItem>
                <MenuItem value={DifficultMap.INTERMEDIATE}>{DifficultMap.INTERMEDIATE}</MenuItem>
                <MenuItem value={DifficultMap.EXPERT}>{DifficultMap.EXPERT}</MenuItem>
            </Select>
        </FormControl>
    );
};