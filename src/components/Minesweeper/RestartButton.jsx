import faces from "./face.module.css";
import {useState} from "react";
import classnames from "classnames";

const FaceStylesMap = {
    IDLE: faces.f_idle,
    PRESSED: faces.f_pressed,
    FRIGHTENED: faces.f_frightened,
    LOSE: faces.f_lose
};

export const RestartButton = (props) => {
    const size = props.size;
    const onClick = props.onClick;
    const [faceStyle, setFaceStyle] = useState(FaceStylesMap.IDLE);

    const handleMouseDown = (event) => {
        if (event.button === 0) {
            setFaceStyle(FaceStylesMap.PRESSED);
        }
    }

    const handleMouseUp = (event) => {
        if (event.button === 0) {
            setFaceStyle(FaceStylesMap.IDLE);
        }
    }

    const handleMouseLeave = (event) => {
        setFaceStyle(FaceStylesMap.IDLE);
    }

    return (
        <div
            onClick={onClick}
            onMouseDown={handleMouseDown}
            onMouseUp={handleMouseUp}
            onMouseLeave={handleMouseLeave}
            className={classnames(faces.face, faceStyle)}
            style={{width: `${size * 1.625}px`, height: `${size * 1.625}px`}}></div>
    )
}