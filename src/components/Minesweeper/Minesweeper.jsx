import React, {useEffect, useState} from "react";
import {Minefield} from "../Minefield/Minefield";
import {fieldStateHandle, restartHandle} from "../../dev/Requests";
import minesweeperStyles from './minesweeper.module.css'
import classnames from "classnames";
import {Timer} from "./Timer";
import {RestartButton} from "./RestartButton";
import {DIFFICULT_COOKIE_KEY, DifficultSelector} from "./DifficultSelector";
import {useCookies} from "react-cookie";

export const Minesweeper = () => {
    const defaultFieldStateJson = "{\"width\":10,\"height\":10,\"state\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}";
    const defaultFieldState = JSON.parse(defaultFieldStateJson);
    const [fieldState, setFieldState] = useState(defaultFieldState);
    const [cookies, setCookie] = useCookies([DIFFICULT_COOKIE_KEY]);
    const defaultSize = 24;
    const size = defaultSize;
    const numMinesLeft = 99;
    const secondsHavePassed = 1309;

    const restartGame = async (difficult = "default") => {
        const newFieldStateJson = await restartHandle(difficult !== "default" ? difficult : cookies.difficult);
        if (newFieldStateJson === undefined) {
            return
        }

        const newFieldState = JSON.parse(newFieldStateJson);
        setFieldState(newFieldState);
    }

    useEffect(() => {
        const updateState = async () => {
            const testMode = false;
            if (testMode) {
                const testFieldStateTestJson = "{\"width\":15,\"height\":10,\"state\":[0,5,1,1,1,1,1,1,1,1,5,0,0,0,0,5,5,1,1,1,1,5,5,5,1,6,0,0,0,0,1,1,1,1,1,1,5,0,6,5,6,0,0,0,0,1,1,1,1,5,5,6,0,0,0,0,0,0,0,0,1,1,1,1,5,0,0,0,0,0,0,0,0,0,0,1,5,5,5,5,0,0,0,0,0,0,0,0,0,0,5,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}";
                const testFieldStateTest = JSON.parse(testFieldStateTestJson);
                setFieldState(testFieldStateTest);
            } else {
                const newFieldStateJson = await fieldStateHandle();
                if (newFieldStateJson === undefined) {
                    return
                }

                const newFieldState = JSON.parse(newFieldStateJson);
                setFieldState(newFieldState);
            }
        }

        updateState();
    }, [])

    return (
        <div style={{
            display: "flex",
            flexDirection: "column",
            width: `${size * (2 + fieldState.width)}px`,
            height: `${size * (8 + fieldState.height)}px`
        }}>
            <DifficultSelector onDifficultChange={restartGame}/>
            <div style={{display: "flex"}}>
                <div
                    className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_corner_up_left)}
                    style={{width: `${size}px`, height: `${size}px`}}/>
                <div className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_border_hor)}
                     style={{height: `${size}px`, flexGrow: 1}}/>
                <div
                    className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_corner_up_right)}
                    style={{width: `${size}px`, height: `${size}px`}}/>
            </div>
            <div style={{display: "flex", backgroundColor: "#C0C0C0"}}>
                <div className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_border_vert)}
                     style={{width: `${size}px`}}/>
                <div style={{
                    display: "flex",
                    flexGrow: 1,
                    height: `${size * 2}px`,
                    justifyContent: "space-between",
                    alignItems: "center"
                }}>
                    <div style={{display: "flex"}}>
                        <div style={{width: `${size * 0.1875}px`}}/>
                        <Timer size={size} value={numMinesLeft}/>
                    </div>
                    <RestartButton size={size} onClick={() => restartGame()}/>
                    <div style={{display: "flex"}}>
                        <Timer size={size} value={secondsHavePassed}/>
                        <div style={{width: `${size * 0.1875}px`}}/>
                    </div>
                </div>
                <div className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_border_vert)}
                     style={{width: `${size}px`}}/>
            </div>
            <div style={{display: "flex"}}>
                <div className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_t_left)}
                     style={{width: `${size}px`, height: `${size}px`}}/>
                <div className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_border_hor)}
                     style={{height: `${size}px`, flexGrow: 1}}/>
                <div className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_t_right)}
                     style={{width: `${size}px`, height: `${size}px`}}/>
            </div>
            <div style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "flex-start",
                alignContent: "flex-start"
            }}>
                <div className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_border_vert)}
                     style={{width: `${size}px`, alignSelf: "stretch"}}/>
                <Minefield state={fieldState} cellSize={defaultSize}/>
                <div className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_border_vert)}
                     style={{width: `${size}px`, alignSelf: "stretch"}}/>
            </div>
            <div style={{display: "flex", flexDirection: "row", alignSelf: "stretch"}}>
                <div
                    className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_corner_bottom_left)}
                    style={{width: `${size}px`, height: `${size}px`}}/>
                <div className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_border_hor)}
                     style={{height: `${size}px`, flexGrow: 1}}/>
                <div
                    className={classnames(minesweeperStyles.minesweeper_border, minesweeperStyles.mb_corner_bottom_right)}
                    style={{width: `${size}px`, height: `${size}px`}}/>
            </div>
        </div>
    )
}