import classnames from "classnames";
import digits from "./digits.module.css"

const DigitsMap = {
    0: digits.d_0,
    1: digits.d_1,
    2: digits.d_2,
    3: digits.d_3,
    4: digits.d_4,
    5: digits.d_5,
    6: digits.d_6,
    7: digits.d_7,
    8: digits.d_8,
    9: digits.d_9,
};

export const Timer = (props) => {
    const size = props.size;
    const value = props.value;
    const roundedValue = Math.round(value);
    const isValueInRange = roundedValue <= 999;
    const rightDigit = isValueInRange ? Math.trunc((roundedValue) % 10) : 9;
    const centerDigit = isValueInRange ? Math.trunc((roundedValue / 10) % 10) : 9;
    const leftDigit = isValueInRange ? Math.trunc((roundedValue / 100) % 10) : 9;

    return (
        <div className={classnames(digits.digits_back)} style={{
            width: `${size * 2.5625}px`,
            height: `${size * 1.5625}px`,
            display: "flex",
            flexDirection: "column",
            alignItems: "stretch"
        }}>
            <div style={{height: `${size * 0.125}px`}}></div>
            <div style={{display: "flex", justifyContent: "center", flexGrow: 1}}>
                <div style={{width: `${size * 0.125}px`}}></div>
                <div className={classnames(digits.digit, DigitsMap[leftDigit])} style={{flexGrow: 1}}></div>
                <div className={classnames(digits.digit, DigitsMap[centerDigit])} style={{flexGrow: 1}}></div>
                <div className={classnames(digits.digit, DigitsMap[rightDigit])} style={{flexGrow: 1}}></div>
                <div style={{width: `${size * 0.125}px`}}></div>
            </div>
            <div style={{height: `${size * 0.125}px`}}></div>
        </div>
    )
}