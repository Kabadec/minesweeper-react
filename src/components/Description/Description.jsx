import {Link} from "@mui/material";
import Typography from "@mui/material/Typography";

export const Description = () => {
    return (
        <div style={{display: "flex", flexDirection: "column", justifyContent: "flex-start", alignItems: "flex-start"}}>
            <Typography variant="h5" gutterBottom sx={{mt: 0, mb: 0}}>
                Описание
            </Typography>
            <p>Пет-проект для портфолио, который повторяет оригинальный Сапёр из WindowsXP. Все вычисления матча
                происходят на беке ASP.NET, данные по матчу хранятся в MsSql.</p>
            <Typography variant="h6" gutterBottom sx={{mt: 0, mb: -1}}>
                Стек
            </Typography>
            <ul>
                <li><b>Бек</b>: C#, ASP.NET, MsSql, Entity Framework<br/><Link
                    href="https://gitlab.com/Kabadec/minesweeper-asp.net"
                    underline="always">{'https://gitlab.com/Kabadec/minesweeper-asp.net'}</Link></li>
                <br/>
                <li><b>Фронт</b>: JavaScript, React, Material UI<br/><Link
                    href="https://gitlab.com/Kabadec/minesweeper-react"
                    underline="always">{'https://gitlab.com/Kabadec/minesweeper-react'}</Link></li>
            </ul>
        </div>)
}