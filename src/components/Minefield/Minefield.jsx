import classnames from "classnames";
import minefield from './minefield.module.css'
import {Cell, CellTypeConsts} from "../Cells/Cell";
import {useEffect, useState} from "react";
import {markCellHandle, openCellHandle} from "../../dev/Requests";

export const Minefield = (props) => {
    const [width, setWidth] = useState(props.state.width);
    const [height, setHeight] = useState(props.state.height);
    const [fieldWidth, setFieldWidth] = useState(width * props.cellSize);
    const [fieldHeight, setFieldHeight] = useState(height * props.cellSize);
    const [cells, setCells] = useState(props.state.state);
    const [isInputEnable, setIsInputEnable] = useState(true);
    const [selectedCells, setSelectedCells] = useState(new Set());
    const [centerSelectedCellsIndex, setCenterSelectedCellsIndex] = useState(-1);
    const [isLeftMouseDown, setIsLeftMouseDown] = useState(false);

    const fieldChangesJsonTest = "{\"isFieldChanged\":true,\"cellsChangeArgs\":[{\"x\":1,\"y\":1,\"state\":1},{\"x\":0,\"y\":1,\"state\":1},{\"x\":0,\"y\":2,\"state\":5},{\"x\":0,\"y\":0,\"state\":1},{\"x\":1,\"y\":0,\"state\":1},{\"x\":2,\"y\":0,\"state\":5},{\"x\":2,\"y\":1,\"state\":5},{\"x\":1,\"y\":2,\"state\":5},{\"x\":2,\"y\":2,\"state\":1},{\"x\":1,\"y\":3,\"state\":5},{\"x\":2,\"y\":3,\"state\":1},{\"x\":1,\"y\":4,\"state\":5},{\"x\":2,\"y\":4,\"state\":1},{\"x\":1,\"y\":5,\"state\":1},{\"x\":0,\"y\":5,\"state\":1},{\"x\":0,\"y\":6,\"state\":1},{\"x\":0,\"y\":7,\"state\":1},{\"x\":0,\"y\":8,\"state\":1},{\"x\":0,\"y\":9,\"state\":1},{\"x\":1,\"y\":9,\"state\":5},{\"x\":1,\"y\":8,\"state\":5},{\"x\":1,\"y\":7,\"state\":5},{\"x\":1,\"y\":6,\"state\":1},{\"x\":2,\"y\":6,\"state\":1},{\"x\":2,\"y\":7,\"state\":5},{\"x\":2,\"y\":5,\"state\":1},{\"x\":3,\"y\":5,\"state\":1},{\"x\":3,\"y\":6,\"state\":1},{\"x\":3,\"y\":7,\"state\":5},{\"x\":4,\"y\":6,\"state\":1},{\"x\":4,\"y\":7,\"state\":1},{\"x\":3,\"y\":8,\"state\":5},{\"x\":4,\"y\":8,\"state\":1},{\"x\":3,\"y\":9,\"state\":5},{\"x\":4,\"y\":9,\"state\":1},{\"x\":5,\"y\":9,\"state\":5},{\"x\":5,\"y\":8,\"state\":6},{\"x\":5,\"y\":7,\"state\":6},{\"x\":5,\"y\":6,\"state\":6},{\"x\":4,\"y\":5,\"state\":1},{\"x\":3,\"y\":4,\"state\":1},{\"x\":3,\"y\":3,\"state\":1},{\"x\":3,\"y\":2,\"state\":5},{\"x\":4,\"y\":3,\"state\":1},{\"x\":4,\"y\":4,\"state\":1},{\"x\":5,\"y\":4,\"state\":1},{\"x\":5,\"y\":5,\"state\":5},{\"x\":5,\"y\":3,\"state\":1},{\"x\":4,\"y\":2,\"state\":5},{\"x\":5,\"y\":2,\"state\":5},{\"x\":6,\"y\":3,\"state\":1},{\"x\":6,\"y\":4,\"state\":1},{\"x\":6,\"y\":5,\"state\":5},{\"x\":7,\"y\":4,\"state\":1},{\"x\":7,\"y\":5,\"state\":5},{\"x\":7,\"y\":3,\"state\":5},{\"x\":8,\"y\":4,\"state\":1},{\"x\":8,\"y\":5,\"state\":1},{\"x\":7,\"y\":6,\"state\":6},{\"x\":8,\"y\":6,\"state\":5},{\"x\":9,\"y\":5,\"state\":1},{\"x\":9,\"y\":6,\"state\":5},{\"x\":9,\"y\":4,\"state\":5},{\"x\":10,\"y\":5,\"state\":1},{\"x\":10,\"y\":6,\"state\":6},{\"x\":10,\"y\":4,\"state\":5},{\"x\":11,\"y\":5,\"state\":1},{\"x\":11,\"y\":6,\"state\":5},{\"x\":11,\"y\":4,\"state\":5},{\"x\":12,\"y\":5,\"state\":6},{\"x\":12,\"y\":4,\"state\":5},{\"x\":12,\"y\":6,\"state\":6},{\"x\":8,\"y\":3,\"state\":5},{\"x\":9,\"y\":3,\"state\":6},{\"x\":6,\"y\":2,\"state\":1},{\"x\":5,\"y\":1,\"state\":6},{\"x\":6,\"y\":1,\"state\":5},{\"x\":7,\"y\":2,\"state\":5},{\"x\":7,\"y\":1,\"state\":5},{\"x\":0,\"y\":4,\"state\":5},{\"x\":3,\"y\":1,\"state\":6}]}";

    const handleLeftMouseButtonDown = (index) => {
        setIsLeftMouseDown(true);
        selectCell(index);
    };

    const handleLeftMouseButtonUp = async (index) => {
        if (!isInputEnable) {
            return;
        }

        setIsInputEnable(false);
        deselectAllCells();
        setIsLeftMouseDown(false);
        const coords = convertIndexToCoordinates(index);
        const cellsChanges = await openCellHandle(coords.x, coords.y);
        if (cellsChanges !== undefined) {
            applyFieldChanges(cellsChanges);
        }

        setIsInputEnable(true);
    };

    const handleRightMouseButtonDown = async (index) => {
        if (!isInputEnable) {
            return;
        }

        setIsInputEnable(false);
        const coords = convertIndexToCoordinates(index);
        const cellsChanges = await markCellHandle(coords.x, coords.y);
        if (cellsChanges !== undefined) {
            applyFieldChanges(cellsChanges);
        }

        setIsInputEnable(true);
    };

    const handleMouseEnter = (index) => {
        if (isLeftMouseDown) {
            selectCell(index);
        }
    };

    const handleMouseLeave = (index) => {
        // if(index === centerSelectedCellsIndex){
        //     deselectAllCells();
        // }
    };

    const handleMouseLeaveOnField = () => {
        deselectAllCells();
        setIsLeftMouseDown(false);
    }

    const applyFieldChanges = (fieldChangesJson) => {
        const fieldChanges = JSON.parse(fieldChangesJson);
        if (!fieldChanges.isFieldChanged) {
            return;
        }

        const newCells = [...cells];
        fieldChanges.cellsChangeArgs.forEach((cellChanges) => {
            const index = cellChanges.y * width + cellChanges.x;
            newCells[index] = cellChanges.state;
        });

        setCells(newCells);
    };

    const selectCell = (index) => {
        const newSelectedCells = new Set();

        const isCellOpened = (index) => {
            const cellState = cells[index];
            return cellState === CellTypeConsts.IDLE_OPENED ||
                cellState === CellTypeConsts.DIGIT_ONE ||
                cellState === CellTypeConsts.DIGIT_TWO ||
                cellState === CellTypeConsts.DIGIT_THREE ||
                cellState === CellTypeConsts.DIGIT_FOUR ||
                cellState === CellTypeConsts.DIGIT_FIVE ||
                cellState === CellTypeConsts.DIGIT_SIX ||
                cellState === CellTypeConsts.DIGIT_SEVEN ||
                cellState === CellTypeConsts.DIGIT_EIGHT;
        }

        const isCellNeedToSelect = (index) => {
            return cells[index] === CellTypeConsts.IDLE_CLOSED;
        };

        const selectCellsAroundCell = (index) => {
            const coords = convertIndexToCoordinates(index);
            const x = coords.x;
            const y = coords.y;
            const validateCoordinates = (x, y) => {
                return !(x < 0 || x >= width || y < 0 || y >= height);
            };
            const tryAddCellToSelected = (x, y) => {
                const index = y * width + x;
                if (validateCoordinates(x, y) && isCellNeedToSelect(index)) {
                    newSelectedCells.add(index)
                }
            };

            tryAddCellToSelected(x - 1, y);
            tryAddCellToSelected(x - 1, y - 1);
            tryAddCellToSelected(x - 1, y + 1);
            tryAddCellToSelected(x, y + 1);
            tryAddCellToSelected(x, y - 1);
            tryAddCellToSelected(x + 1, y);
            tryAddCellToSelected(x + 1, y - 1);
            tryAddCellToSelected(x + 1, y + 1);
        }

        if (isCellNeedToSelect(index)) {
            newSelectedCells.add(index)
            setSelectedCells(newSelectedCells);
            setCenterSelectedCellsIndex(index);
        } else if (isCellOpened(index)) {
            selectCellsAroundCell(index);
            setSelectedCells(newSelectedCells);
            setCenterSelectedCellsIndex(index);
        }
    };

    const deselectAllCells = () => {
        setCenterSelectedCellsIndex(-1);
        setSelectedCells(new Set());
    };

    const convertIndexToCoordinates = (index) => {
        const x = index % width;
        const y = Math.floor(index / width);
        return {x, y};
    };

    useEffect(() => {
        setCells(props.state.state);
        setWidth(props.state.width);
        setHeight(props.state.height);
        setFieldWidth(props.state.width * props.cellSize);
        setFieldHeight(props.state.height * props.cellSize);
        setSelectedCells(new Set())
    }, [props.state, props.state.width, props.state.height, props.cellSize]);

    return (
        <>
            <div
                onContextMenu={(event) => event.preventDefault()}
                className={classnames(minefield.field)}
                style={{width: `${fieldWidth}px`, height: `${fieldHeight}px`}}
                onMouseLeave={() => handleMouseLeaveOnField()}
            >
                {cells.map((cell, index) => {
                        const coords = convertIndexToCoordinates(index);
                        const key = `cell_${coords.x}_${coords.y}`;
                        return (
                            <Cell
                                key={key}
                                id={key}
                                state={cell}
                                size={props.cellSize}
                                isSelected={selectedCells.has(index)}
                                onLeftMouseButtonDown={() => handleLeftMouseButtonDown(index)}
                                onLeftMouseButtonUp={() => handleLeftMouseButtonUp(index)}
                                onRightMouseButtonDown={() => handleRightMouseButtonDown(index)}
                                onMouseEnter={() => handleMouseEnter(index)}
                                onMouseLeave={() => handleMouseLeave(index)}
                            />);
                    }
                )}
            </div>
        </>

    )
}