import cellTypes from './cellTypes.module.css'
import cellStyles from './cellStyles.module.css'
import classnames from "classnames";

export const CellTypeConsts = {
    IDLE_CLOSED: 0,
    IDLE_OPENED: 1,
    MARKED: 2,
    MINE: 3,
    EXPLODED_MINE: 4,
    DIGIT_ONE: 5,
    DIGIT_TWO: 6,
    DIGIT_THREE: 7,
    DIGIT_FOUR: 8,
    DIGIT_FIVE: 9,
    DIGIT_SIX: 10,
    DIGIT_SEVEN: 11,
    DIGIT_EIGHT: 12,
};

const CellStyleMap = {
    0: cellTypes.ct_idle_closed,
    1: cellTypes.ct_idle_opened,
    2: cellTypes.ct_marked,
    3: cellTypes.ct_mine,
    4: cellTypes.ct_exploded_mine,
    5: cellTypes.ct_digit_one,
    6: cellTypes.ct_digit_two,
    7: cellTypes.ct_digit_three,
    8: cellTypes.ct_digit_four,
    9: cellTypes.ct_digit_five,
    10: cellTypes.ct_digit_six,
    11: cellTypes.ct_digit_seven,
    12: cellTypes.ct_digit_eight
};

export const Cell = (props) => {
    const size = props.size;
    const state = props.state;
    const cellId = props.id;
    const isSelected = props.isSelected;

    const handleLeftMouseButtonUp = (event) => {
        if (event.button === 0) {
            props.onLeftMouseButtonUp();
        }
    };

    const handleRightMouseButtonDown = (event) => {
        if (event.button === 0) {
            props.onLeftMouseButtonDown();
        }

        if (event.button === 2) {
            props.onRightMouseButtonDown();
        }
    };

    const handleRightMouseEnter = (event) => {
        props.onMouseEnter();
    };

    const handleRightMouseExit = (event) => {
        props.onMouseLeave();
    };

    return (
        <div
            id={cellId}
            className={classnames(cellStyles.cell_style, (isSelected ? cellTypes.ct_selected : CellStyleMap[state]))}
            style={{width: `${size}px`, height: `${size}px`}}
            onMouseUp={handleLeftMouseButtonUp}
            onMouseDown={handleRightMouseButtonDown}
            onMouseEnter={handleRightMouseEnter}
            onMouseLeave={handleRightMouseExit}>
        </div>
    )
}