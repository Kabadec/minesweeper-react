import {DifficultMap} from "../components/Minesweeper/DifficultSelector";

export const openCellHandle = async (x, y) => {
    return await handleRequest(`/minesweeper/opencell?x=${x}&y=${y}`);
};

export const markCellHandle = async (x, y) => {
    return await handleRequest(`/minesweeper/markcell?x=${x}&y=${y}`);
};

export const fieldStateHandle = async () => {
    return await handleRequest(`/minesweeper/fieldstate`);
};

export const restartHandle = async (difficult = DifficultMap.BEGINNER) => {
    return await handleRequest(`/minesweeper/restart?difficult=${difficult}`);
};

const handleRequest = async (route) => {
    try {
        const responseTime = Date.now();
        const response = await fetch(route, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const data = await response.json();
        const requestTime = Date.now();
        const pingTime = requestTime - responseTime;
        console.log('Ping к серверу:', pingTime, 'мс');
        return JSON.stringify(data);
    } catch (error) {
        console.error('Ошибка при отправке запроса:', error);
        return undefined;
    }
};